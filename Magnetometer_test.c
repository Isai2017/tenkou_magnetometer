/*
 * File:   Magnetometer_test.c
 * Author: Isai
 *
 * Created on April 10, 2018, 4:42 PM
 */

// PIC16F877 Configuration Bit Settings
// 'C' source line config statements
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF        // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

#define _XTAL_FREQ     20000000

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "magnetometer.h"


#define UART_BAUD 9600 // UART baud rate in bits per second.
#define SPI_BUF_SIZE 16 // Size of the read and write SPI buffers in bytes.
/*
 * Debugging data bytes and information.
 */
//unsigned char loopStart[] = {"START"}; //use "START" as start characters.
//unsigned char MagMsg[]={"MAG"}; // Beginin of reading data from magnetometer.
//unsigned char spiRData[SPI_BUF_SIZE] = {'0'}; // Array to store received SPI data.

unsigned char cr = 0x0D; // New line.
//unsigned char uartNewline[] = {"\n"}; // Distinct end of debug messages.
unsigned char uartSpace[] = {" "}; // To separate consecutive readings.
unsigned char uartTerminator[] = {"X\n"}; // Distinct end of debug messages.
uint8_t rx_data[2];
uint8_t data_mag[12];
float converting_data[6];
int status;
char* buf;
uint8_t ch1[4] = {'C','H','1',':'};
    uint8_t ch2[4] = {'C','H','2',':'};
    uint8_t ch3[4] = {'C','H','3',':'};
    uint8_t adc1[5] = {'a','d','c','1','_'};
    uint8_t adc2[5] = {'a','d','c','2','_'};
// Data bytes to send to the GYROS.
//*****************************************************************************


int test(void) {
    decoder_initialize();
    spiMasterInit(); // Starts the SPI as master. FROM magnetometer.c
    uartInit(UART_BAUD);//From read_magnetometer
    SPI_init_pin();
    for(;;){
        
        MISO_SWITCH_INIT = 0;
        MISO_SWITCH = 1;
                        
        read_magnetometer(data_mag);
       
        //Converting data to float       
        
        for(int time = 0 ; time < 3 ; time++) {
        rx_data[0]=data_mag[time * 2];
        rx_data[1]=data_mag[(time * 2) + 1];
        converting_data[time] = conversion_voltage(rx_data);
        rx_data[0]=data_mag[(time * 2) + 6];
        rx_data[1]=data_mag[(time * 2) + 7];
        converting_data[time + 3] = conversion_voltage(rx_data);
        }
    
    // Sending converting data from ADC1 CH1
        //converting_adc16(&adc_1[0], rx_data);
        
        //converting_data[0] = conversion_voltage(rx_data);
           
        buf = ftoa(converting_data[0], &status);
        //uartWriteBytes(adc1,5);
        //uartWriteBytes(ch1,4);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        //uartWrite(&cr);
               
        // Sending converting data from ADC1 CH2
        //converting_adc16(&adc_1[1], rx_data);
        
        //converting_data[0] = conversion_voltage(rx_data);
        
        buf = ftoa(converting_data[1], &status);
        //uartWriteBytes(adc1,5);
       //uartWriteBytes(ch2,4);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        //uartWrite(&cr);
       
        // Sending converting data from ADC1 CH3
        //converting_adc16(&adc_1[2], rx_data);
        
        //converting_data[0] = conversion_voltage(rx_data);
        
        buf = ftoa(converting_data[2], &status);
        //uartWriteBytes(adc1,5);
        //uartWriteBytes(ch3,4);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        //uartWrite(&cr);
       
        // READ AND SEND MAGNETOMETER ADC2 DATA FROM 3 CHANNES
        
        // Sending converting data from ADC2 CH1
        //converting_adc16(&adc_2[0], rx_data);
        
        //converting_data[0] = conversion_voltage(rx_data);
        
        buf = ftoa(converting_data[3], &status);
        uartWriteBytes(adc2,5);
        uartWriteBytes(ch1,4);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        //uartWrite(&cr);
        
        // Sending converting data from ADC2 CH2
        //converting_adc16(&adc_2[1], rx_data);
        
        //converting_data[0] = conversion_voltage(rx_data);
        
        buf = ftoa(converting_data[4], &status);
        uartWriteBytes(adc2,5);
        uartWriteBytes(ch2,4);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        //uartWrite(&cr);
        
        // Sending converting data from ADC2 CH3
        //converting_adc16(&adc_2[2], rx_data);
        
        //converting_data[0] = conversion_voltage(rx_data);
        
        buf = ftoa(converting_data[5], &status);
        uartWriteBytes(adc2,5);
        uartWriteBytes(ch3,4);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        uartWrite(&cr);
        
        //uartWriteBytes(end,3);
        //uartWrite(&cr);
                       
    }
}
/*
int test_board(void) {
    TRISC7 = 0;
    for (;;) {
        RC7 = 1;
    }
}
*/

int main(void){
    uartInit(UART_BAUD); // Initialize UART @ 9600 baud.
    //uartWriteBytes(loopStart,5);
    
    int a = test();
    //int b = read_magnetometer();
    //int c = test_board();
}

