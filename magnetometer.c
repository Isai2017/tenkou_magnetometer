#include "magnetometer.h"
/*
 *Disable unnecessary CS for mag ADC1   
 * 
 */
void dis_allCS_MAG(void) {

SPI_SENSE_CS_SP2_2 = 1; 
SPI_SENSE_CS_SP2_4 = 1;
SPI_SENSE_CS_SP1_2 = 1;
SPI_SENSE_CS_SP2_3 = 1;
SPI_SENSE_CS_SP1_3 = 1;
SPI_SENSE_CS_SP1_6 = 1;
SPI_SENSE_CS_SP1_5 = 1;
SPI_SENSE_CS_SP2_5 = 1;
SPI_SENSE_CS_SP1_1 = 1;
SPI_SENSE_CS_SP2_1 = 1;
SPI_SENSE_CS_SP2_6 = 1;
SPI_SENSE_CS_SP1_4 = 1;
SPI_SENSE_CS_SD = 1;
SPI_SENSE_CS_GYRO = 1;

}
/*
 *Function to SET/RESET the magnetometer by a current pulse of ~4 Amp.
 */
//*****************************************************************************
int mag_set_reset_(void)
{
    
    // This sets digital signals for SET and RESET in the magnetometer
    // TRS >= 5 usec
    // TSR >= 5 usec
    // TPW ~2 usec 
    //uartWriteBytes(loopStart,5);
    //uartWriteBytes(uartTerminator,2);
    MAG_RESET = 1; // Initial and default value is high or one.
    MAG_SET = 0; // Initial and default value is zero or low.
    __delay_us(10); // Wait for 10 usec before changing states.
    
    MAG_RESET = 0;
    __delay_us(5); // Waits for TRS seconds before changing the SET signal.
    MAG_SET = 1;
    __delay_us(25); // Waits for 25 usec before returning again to default values (no current pulses).
    MAG_SET = 0; // Returns to its default state (low).
    __delay_us(5); // Waits for TSR seconds before changing the RESET signal.
    MAG_RESET = 1; // Returns to its default state (high).
    
    __delay_us(100); // Delay of 100us. 
    
    return 0;
}
//******************************************************************************
// Testing function for DACX, DACY and DACZ for magnetic offset generation.
//*****************************************************************************
int mag_offset(uint16_t x, uint16_t y, uint16_t z){
    
    decoder_select_pin(7); // Select all CS from decoder to 1 (high).
    
    // We define the DAC control variable (for DLP sweep): 
    unsigned char bytes[2] = {0}; // Dummy to send two bytes to the DAC over SPI.
        
    // Change the value of i to 0 if using the ramp generation test (for loop code).
    // comment from here...
    //uint24_t i = 31458; // Define a 24bit variable to overcome the overflow in the for loop (testing ramp output).
    
    bytes[0]=(x>>8)&0xFF;
    bytes[1]=x&0xFF;
                // Send offset value to DAC_X.
    decoder_select_pin(0); // Bring selected DAC CS to low (DAC_X to low). 
    spiMasterWrite(0x00); // Send first byte of zeros to the DAC.
    spiMasterWriteBytes(bytes,2); // The DAC output, which defines the DAC's output voltage.
    decoder_select_pin(7); // Bring the DAC CS high.
    __delay_us(5);
                // Send offset value to DAC_Y.
    bytes[0]=(y>>8)&0xFF;
    bytes[1]=y&0xFF;
    decoder_select_pin(1); // Bring selected DAC CS to low (DAC_Y to low). 
    spiMasterWrite(0x00); // Send first byte of zeros to the DAC.
    spiMasterWriteBytes(bytes,2); // The DAC output, which defines the DAC's output voltage.
    decoder_select_pin(7); // Bring the DAC CS high.
    __delay_us(5);
                // Send offset value to DAC_Z.
    bytes[0]=(z>>8)&0xFF;
    bytes[1]=z&0xFF;
    decoder_select_pin(2); // Bring selected DAC CS to low (DAC_Z to low). 
    spiMasterWrite(0x00); // Send first byte of zeros to the DAC.
    spiMasterWriteBytes(bytes,2); // The DAC output, which defines the DAC's output voltage.
    decoder_select_pin(7); // Bring the DAC CS high.
    // ...until here to test ramp output from selected DACs.
    
    /*
    // Testing a ramp output from decoder between 0 and 2.5V
    for( i=0; i<65535; i+=1000)// Change the increment for timing (i+=5) 0xFFFF =>65535.
    {
        // Reset the SPI MASTER pin status modified by the ADC
        SPI_MOSI_MASTER = 0;
        SPI_CK_MASTER = 0;
        
        // Change the bias voltage by changing the DAC output.
        bytes[0]=(i>>8)&0xFF;
        bytes[1]=i&0xFF;
        decoder_select_pin(2); // Bring CS to low in decoder from ADS board. 0 = DAC_X, 1 = DAC_Z, 2 = DAC_Y.    
        spiMasterWrite(0x00); // Send first byte of zeros.   
        spiMasterWriteBytes(bytes,2); // The DAC output, which defines the DAC's output voltage.
        decoder_select_pin(7); // Bring the DAC CS high.
    }
    */   
    return 0;
}
//*****************************************************************************
//******************************************************************************
// Testing of Magnetometer ADC1 and ADC2
void read_magnetometer(uint8_t* const ent){   
    // Default state (not selecting any device DEV3=1,DEV2=1,DEV1=1).
    decoder_select_pin(7); // Select all CSs from decoder to 1 (high).
    // Next step is initializing the ADC1 and ADC2
    SPI_SENSE_CS_MAGADC = 1; // Set the MAGADC1 to high.
    
    uint8_t rx_data1[2];
        
    // Select the ADC CSs lines for MAGADC1 and MAGADC2
    adc16_communication_information_t  trans_adc_1 , trans_adc_2 , adc_1[3] , adc_2[3];
    
    trans_adc_1.adc.cs_pin = 5; // MAGADC1 CS
        trans_adc_1.adc.cs_port = &PORTD;
        trans_adc_1.adc.q_outpin = 8;
        trans_adc_1.adc.reset_pin = 0;
        trans_adc_1.adc.reset_port = 0;
        
        trans_adc_2.adc.cs_pin = 0;
        trans_adc_2.adc.cs_port = 0;
        trans_adc_2.adc.q_outpin = 4; // MAGADC2 CS
        trans_adc_2.adc.reset_pin = 0;
        trans_adc_2.adc.reset_port = 0;
       
       
        for(int time = 0 ; time < 3 ; time++) {
            adc_1[time] = trans_adc_1;
            adc_2[time] = trans_adc_2;
        }
        
        adc_1[0].convert_channel.channel = 0x00;    //CH1 for ADC1
        adc_1[0].convert_channel.filter = 0x00;
        adc_1[0].convert_channel.gain = 0x00;
        
        adc_1[1].convert_channel.channel = 0x01;    //CH2 for ADC1
        adc_1[1].convert_channel.filter = 0x00;
        adc_1[1].convert_channel.gain = 0x00;
                
        adc_1[2].convert_channel.channel = 0x03;    //CH3 for ADC1
        adc_1[2].convert_channel.filter = 0x00;
        adc_1[2].convert_channel.gain = 0x00;
        
        adc_2[0].convert_channel.channel = 0x00;    //CH1 for ADC2
        adc_2[0].convert_channel.filter = 0x00;
        adc_2[0].convert_channel.gain = 0x00;
        
        adc_2[1].convert_channel.channel = 0x01;    //CH2 for ADC2
        adc_2[1].convert_channel.filter = 0x00;
        adc_2[1].convert_channel.gain = 0x00;
        
        adc_2[2].convert_channel.channel = 0x03;    //CH3 for ADC2
        adc_2[2].convert_channel.filter = 0x00;
        adc_2[2].convert_channel.gain = 0x00;
       
    decoder_select_pin(7);
    SPI_SENSE_CS_MAGADC = 1; // CS(for magnetometer) is High or SPI_SENSE_ADS_CS_MAGADC1
    mag_set_reset_(); // Sends a current pulse for SET/RESET of magnetometer's resistor straps. DO NOT put a magnet close to magnetometer during SET/RESET.
    __delay_us(10);
    SPI_SENSE_CS_MAGADC = 1; // Set MAGADC1 CS to high
    SPI_SENSE_CS_GYRO = 1; // Set GYRO1 CS to high.
    mag_offset(65500,32910,32910);
    // READ MAGNETOMETER ADC1 and ADC2 DATA FROM 3 CHANNELS
    dis_allCS_MAG(); 
    for(int time = 0 ; time < 3 ; time++) {
        converting_adc16(&adc_1[time], rx_data1);
        ent[(time * 2)] = rx_data1[0];
        ent[(time * 2) + 1] = rx_data1[1];
        converting_adc16(&adc_2[time], rx_data1);
        ent[(time * 2) + 6] = rx_data1[0];
        ent[(time * 2) + 7] = rx_data1[1];
        }
           
}
//******************************************************************************