#ifndef MAGNETOMETER_H
#define	MAGNETOMETER_H

//#define DEBUG_LIULIN // To print Lulin data to UART for eyballing.

#include <stdint.h> // For using uint8_t, uint_16t, etc.
#include "TenkouGPIO.h"
#include "spi_master.h"
#include "uart.h"
#include "decoder.h"
#include "adc_16bit.h"
#include "TK-02-02-01_IF_PV.h"
#include "spi_pins.h"

void initialize_(void);
int mag_set_reset_(void);
int mag_offset(uint16_t x, uint16_t y, uint16_t z);
//int read_magnetometer(void);
void read_magnetometer(uint8_t* const ent);

#endif	/* ECU_TEST_H */

